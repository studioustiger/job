package com.tiger.job.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tiger.job.common.entity.SysGlueVersion;

/**
 * 描述：glue版本控制
 *
 * @author huxuehao
 **/
public interface GlueVersionService extends IService<SysGlueVersion> {
}
