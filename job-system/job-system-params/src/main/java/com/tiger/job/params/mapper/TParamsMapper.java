package com.tiger.job.params.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tiger.job.common.entity.Params;
import org.apache.ibatis.annotations.Mapper;

/**
 * 描述：系统参数
 *
 * @author huxuehao
 **/
@Mapper
public interface TParamsMapper extends BaseMapper<Params> {
}
