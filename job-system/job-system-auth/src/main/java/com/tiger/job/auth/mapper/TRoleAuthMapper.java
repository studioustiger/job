package com.tiger.job.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tiger.job.common.entity.RoleAuth;
import org.apache.ibatis.annotations.Mapper;

/**
 * 描述：角色权限
 *
 * @author huxuehao
 **/
@Mapper
public interface TRoleAuthMapper extends BaseMapper<RoleAuth> {
}
