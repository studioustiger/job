package com.tiger.job.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tiger.job.common.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * 描述：用户角色
 *
 * @author huxuehao
 **/
@Mapper
public interface TUserRoleMapper extends BaseMapper<UserRole> {
}
