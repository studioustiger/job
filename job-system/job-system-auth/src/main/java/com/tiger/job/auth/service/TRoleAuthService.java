package com.tiger.job.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tiger.job.common.entity.RoleAuth;

/**
 * 描述：角色权限
 *
 * @author huxuehao
 **/
public interface TRoleAuthService extends IService<RoleAuth> {
}
