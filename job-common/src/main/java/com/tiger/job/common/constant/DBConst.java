package com.tiger.job.common.constant;

/**
 * 描述：数据库常量
 *
 * @author huxuehao
 **/
public class DBConst {
    public static final String DB = "`dcs-job`";
    public static final String SYS_SCHEDULED = DB + ".sys_scheduled";
    public static final String SYS_GLUE_VERSION = DB + ".sys_glue_version";
    public static final String SYS_SCHEDULED_LOG = DB + ".sys_scheduled_log";
    public static final String T_ORGANIZATION = DB + ".sys_organization";
    public static final String T_MENU = DB + ".sys_menu";
    public static final String SYS_CLASSIFY = DB + ".sys_classify";
    public static final String T_MENU_BUTTON = DB + ".sys_menu_button";
    public static final String T_MENU_API = DB + ".sys_menu_api";
    public static final String T_ROLE = DB + ".sys_role";
    public static final String T_ROLE_AUTH = DB + ".sys_role_auth";
    public static final String T_USER_ROLE = DB + ".sys_user_role";
    public static final String T_USER = DB + ".sys_user";
    public static final String T_PARAMS = DB + ".sys_params";
}
