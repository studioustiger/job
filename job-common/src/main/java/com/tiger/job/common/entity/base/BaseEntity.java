package com.tiger.job.common.entity.base;

import java.io.Serializable;

/**
 * 描述：基础entity
 *
 * @author huxuehao
 **/
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = -1;
}
